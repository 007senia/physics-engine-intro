using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Unity.VisualScripting;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class Superman : MonoBehaviour
{
    public float Power;
    public float SupermanSpeed;
    void Start()
    {
        
    }

    private void FixedUpdate()
    {
        SupermanMovement();

    }

    private void SupermanMovement()
    {
        GetComponent<Rigidbody>().AddForce(0,0,SupermanSpeed, ForceMode.VelocityChange);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Rigidbody Rigidcomponent = collision.gameObject.GetComponent<Rigidbody>();
        if (Rigidcomponent != null) 
        {
            Vector3 direction = collision.gameObject.transform.position - transform.position;
            collision.gameObject.GetComponent<Rigidbody>().AddForce(direction.normalized * Power);
            Debug.Log(collision.gameObject.name);
        }
    }
}
