using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Pool : MonoBehaviour
{
    public float Power;
    public GameObject TargetBall;
    // Start is called before the first frame update
    void Start()
    {
        PushBall();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void PushBall()
    {
        Vector3 direction = TargetBall.transform.position - transform.position;
        GetComponent<Rigidbody>().AddForce(direction.normalized * Power, ForceMode.Impulse);
    }    
}
